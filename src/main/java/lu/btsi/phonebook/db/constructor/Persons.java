package lu.btsi.phonebook.db.constructor;

import lu.btsi.phonebook.db.WebDB;
import lu.btsi.phonebook.db.model.Person;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class Persons extends Observable {
    private int currentIndex = 0;
    private Person currentPerson;

    /**
     * Gets person from the current Index.
     *
     * @return Person
     */
    public Person get() {
        Person person = new Person();
        String sql = "SELECT * FROM Person ORDER BY dtName LIMIT " + currentIndex + ",1;";
        try (ResultSet resultSet = WebDB.get().select(sql)) {
            while (resultSet.next()) {
                person = fillRecord(resultSet);
                currentPerson = person;
            }
            Statement stmt = resultSet.getStatement();
            resultSet.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return person;
    }

    /**
     * Adds a new Person to the Database
     *
     * @param name  String
     * @param email String
     * @param phone int
     */
    public void add(String name, String email, int phone) {
        try {
            String sql = "INSERT INTO Person (dtName,dtEmail,dtPhone) VALUES(?,?,?)";
            int personID = WebDB.get().insert(sql, name, email, phone);
            currentIndex = getPersonPositionById(personID) - 1;


        } catch (SQLException e) {
            e.printStackTrace();
        }
        updateObservers();
    }

    /**
     * Removes the Person with the currentIndex from the Database
     */
    public void remove() {
        try {
            String sql = "DELETE FROM Person WHERE idPerson = ?;";
            WebDB.get().query(sql, currentPerson.getId());

            currentIndex = 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates a Person in the Database
     */
    public void update(String name, String email, int phone) {
        try {
            String sql = String.format("UPDATE Person SET dtName = ?, dtEmail = ?, dtPhone = ? WHERE idPerson = ?");
            WebDB.get().query(sql, name, email, phone, currentPerson.getId());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * Searches a person by its id name email or phone
     *
     * @param query search command (example ->  id:12 | name: David Jones)
     * @return Person
     * @throws SQLException
     */
    public Person search(String query) throws SQLException {
        Person person = null;
        if (query.contains(":")) {
            String[] split = query.split(":");
            String sql = null;
            switch (split[0]) {
                case "id":
                    sql = "SELECT * FROM Person WHERE idPerson LIKE  '%' ? '%' LIMIT 1";
                    break;

                case "name":
                    sql = "SELECT * FROM Person WHERE dtName LIKE  '%' ? '%' LIMIT 1";
                    break;

                case "email":
                    sql = "SELECT * FROM Person WHERE dtEmail LIKE  '%' ? '%' LIMIT 1";
                    break;

                case "phone":
                    sql = "SELECT * FROM Person WHERE dtPhone LIKE  '%' ? '%' LIMIT 1";
                    break;
            }
            ResultSet resultSet = WebDB.get().select(sql, split[1]);
            resultSet.next();
            person = fillRecord(resultSet);
            resultSet.close();
            return person;
        } else {
            return null;
        }

    }

    /**
     * Return the current position of the index
     *
     * @return current index
     */
    public int getCurrentIndex() {
        return currentIndex;
    }


    /**
     * Sets the current Person
     *
     * @param currentPerson
     */
    public void setCurrentPerson(Person currentPerson) {
        this.currentPerson = currentPerson;
        this.currentIndex = getPersonPositionById(currentPerson.getId()) - 1;
        updateObservers();
    }

    /**
     * Moves the current index to the next highest index
     */
    public void nextIndex() {
        if (currentIndex < size()) currentIndex++;
    }

    /**
     * Moves the current index to the previous lowest index.
     */
    public void previousIndex() {
        if (currentIndex > 0) currentIndex--;
    }

    /**
     * Returns the arraylist size
     *
     * @return int
     */
    public int size() {
        int count = 0;
        String sql = "SELECT COUNT(*) FROM Person;";

        try (ResultSet resultSet = WebDB.get().select(sql)) {
            resultSet.next();
            count = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }

    /**
     * Fills a new Person Object with the data from ResultSet provided.
     *
     * @param resultSet ResultSet with the Person data
     * @return Person
     * @throws SQLException
     */
    private Person fillRecord(ResultSet resultSet) throws SQLException {
        Person person = new Person();
        person.setId(resultSet.getInt("idPerson"));
        person.setName(resultSet.getString("dtName"));
        person.setEmail(resultSet.getString("dtEmail"));
        person.setPhone(resultSet.getInt("dtPhone"));
        return person;
    }

    /**
     * Return the Persons position in the table ordered by his name
     *
     * @param id Person id
     * @return int
     */
    public int getPersonPositionById(int id) {
        int position = -1;
        String sql = "SELECT position FROM (SELECT\n" +
                "  @i := @i + 1 AS position,\n" +
                "  p.*\n" +
                "FROM Person AS p, (SELECT @i := 0) AS x\n" +
                "ORDER BY p.dtName) AS selection WHERE idPerson = " + id;
        try (ResultSet resultSet = WebDB.get().select(sql)) {
            resultSet.next();
            position = resultSet.getInt("position");
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return position;
    }

    /**
     * Updates all Observers
     */
    private void updateObservers() {
        setChanged();
        notifyObservers();
    }
}
