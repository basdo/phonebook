package lu.btsi.phonebook.db;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import lu.btsi.phonebook.exceptions.UnimplementedParameterException;

import java.sql.*;
import java.util.Calendar;

public class MySQLAdapter {
    private String DB_NAME;
    private String DB_USER;
    private String DB_HOST;
    private String DB_PASSWORD;
    private Connection connection;

    public MySQLAdapter(String host, String databaseUser, String databasePassword, String databaseName) {
        DB_HOST = host;
        DB_USER = databaseUser;
        DB_PASSWORD = databasePassword;
        DB_NAME = databaseName;
    }

    /**
     * Creates a mysql connection to the database
     * More about MysqlConnectionPoolDataSource:
     * https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-j2ee-concepts-connection-pooling.html
     *
     * @return Connection
     */
    private Connection createConnection() {
        try {
            MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
            dataSource.setUser(DB_USER);
            dataSource.setPassword(DB_PASSWORD);
            dataSource.setServerName(DB_HOST);
            dataSource.setPort(3306);
            dataSource.setDatabaseName(DB_NAME);
            dataSource.setZeroDateTimeBehavior("convertToNull");
            dataSource.setUseUnicode(true);
            return dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * Checks if there is any connection if not: creates a new one
     *
     * @return Connection
     */
    public Connection getConnection() {
        if (connection == null) {
            connection = createConnection();
        }
        return connection;
    }

    /**
     * Fetches record form the database
     *
     * @param sql    The sql query
     * @param params Parameters for the prepared statement
     * @return ResultSet
     * @throws SQLException
     */
    public ResultSet select(String sql, Object... params) throws SQLException {
        PreparedStatement query = getConnection().prepareStatement(sql);

        resolveParameters(query, params);
        ResultSet rs = query.executeQuery();
        return rs;
    }


    /**
     * Adds a new record to the database and returns its id
     *
     * @param sql    SQL Query
     * @param params Objects that are required for the prepared statements
     * @return Return the id from the new inserted field. -1 if failed.
     * @throws SQLException
     */
    public int insert(String sql, Object... params) throws SQLException {
        int id = -1;
        try (PreparedStatement query = getConnection().prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            resolveParameters(query, params);
            query.executeUpdate();
            ResultSet rs = query.getGeneratedKeys();

            if (rs.next()) {
                id = rs.getInt(1);
            }

            query.close();
        }
        return id;
    }

    /**
     * Queries the database
     *
     * @param sql    The sql query
     * @param params Parameters for the prepared statement
     * @throws SQLException
     */
    public void query(String sql, Object... params) throws SQLException {
        PreparedStatement query = getConnection().prepareStatement(sql);
        resolveParameters(query, params);
        query.executeUpdate();
        query.close();
    }

    /**
     * Sets the parameters to the prepared statement.
     *
     * @param query  The sql query
     * @param params Parameters for the prepared statement
     * @throws SQLException
     */
    private void resolveParameters(PreparedStatement query, Object... params) throws SQLException {
        int index = 1;
        for (Object param : params) {
            if (param instanceof String) {
                query.setString(index, (String) param);

            } else if (param instanceof Integer) {
                query.setInt(index, (int) param);

            } else if (param instanceof Long) {
                query.setLong(index, (Long) param);

            } else if (param instanceof Double) {
                query.setDouble(index, (double) param);

            } else if (param instanceof java.sql.Date) {
                java.sql.Date d = (java.sql.Date) param;
                Timestamp ts = new Timestamp(d.getTime());
                query.setTimestamp(index, ts);

            } else if (param instanceof java.util.Date) {
                java.util.Date d = (java.util.Date) param;
                Timestamp ts = new Timestamp(d.getTime());
                query.setTimestamp(index, ts);

            } else if (param instanceof Calendar) {
                Calendar cal = (Calendar) param;
                Timestamp ts = new Timestamp(cal.getTimeInMillis());
                query.setTimestamp(index, ts);

            } else if (param == null) {
                query.setNull(index, Types.NULL);

            } else {
                throw new UnimplementedParameterException(param, index);
            }
            index++;
        }
    }
}
