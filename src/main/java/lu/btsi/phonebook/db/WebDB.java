package lu.btsi.phonebook.db;

import lu.btsi.phonebook.core.Config;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class WebDB {

    private static final String DEFAULT_CONNECTION = "default";
    private static HashMap<String, MySQLAdapter> connections = new HashMap<>();

    /**
     * Clears all connections from the HashMap and generate a new 'default' connection.
     */
    public static void init() {
        connections.clear();
        connections.put(DEFAULT_CONNECTION, new MySQLAdapter(Config.dbHost, Config.dbUser, Config.dbPassword, Config.dbName));
    }

    /**
     * Returns the default connection
     *
     * @return MySQLAdapter
     */
    public static MySQLAdapter get() {
        return connections.get(DEFAULT_CONNECTION);
    }


    /**
     * Returns a specific connection
     *
     * @param key Connections name
     * @return MySQLAdapter
     */
    public static MySQLAdapter get(String key) {
        if (connections.containsKey(key)) {
            return connections.get(key);
        } else {
            System.out.println(String.format("The MySQL connection '%s' is not set!", key));
            return null;
        }
    }

    /**
     * Closes each connection in the HashMap connections
     */
    public static void close() {
        for (Map.Entry<String, MySQLAdapter> entry : connections.entrySet()) {
            try {
                entry.getValue().getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
