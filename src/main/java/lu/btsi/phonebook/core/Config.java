package lu.btsi.phonebook.core;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {
    public static String dbHost;
    public static String dbUser;
    public static String dbPassword;
    public static String dbName;

    public static void init() {
        setDBConfig();
    }

    /**
     * Reads the .properties file and fills the variables
     */
    private static void setDBConfig(){
        try(InputStream file = Config.class.getClassLoader().getResourceAsStream("database.properties")){
            Properties properties = new Properties();
            properties.load(file);

            // Assign database parameters
            dbName = properties.getProperty("db");
            dbHost = properties.getProperty("host");
            dbUser = properties.getProperty("user");
            dbPassword = properties.getProperty("password");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
