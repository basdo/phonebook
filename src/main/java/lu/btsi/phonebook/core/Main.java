package lu.btsi.phonebook.core;

import lu.btsi.phonebook.db.WebDB;
import lu.btsi.phonebook.gui.MainFrame;

import javax.swing.*;
import java.awt.*;

public class Main {

    private MainFrame mainFrame;

    public Main() {
        Config.init();
        WebDB.init();

        mainFrame = new MainFrame();
        mainFrame.pack();
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                WebDB.close();
                e.getWindow().dispose();
                System.exit(0);
            }
        });
        mainFrame.setVisible(true);
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(()->{
            new Main();
        });
    }
}
