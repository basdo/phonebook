package lu.btsi.phonebook.gui;

import lu.btsi.phonebook.db.constructor.Persons;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;


public class MainFrame extends JFrame implements Observer {

    private JPanel pane, navigationPanel, formPanel, searchPanel;
    private JButton previousButton, nextButton, newButton, deleteButton, saveButton, searchButton;
    private JLabel navigationLabel, nameLabel, emailLabel, phoneLabel;
    private JTextField nameTextField, emailTextField, phoneTextField, searchTextField;

    private Mode currentMode = Mode.UPDATE;

    private Persons persons;

    public MainFrame() {
        initComponents();
        addListeners();
        configComponents();
        addComponents();
        persons = new Persons();
        persons.addObserver(this);
        updateView();
    }

    public enum Mode {
        UPDATE,
        INSERT
    }

    /**
     * Generates all the UI Components
     */
    private void initComponents() {
        pane = new JPanel(new BorderLayout());
        navigationPanel = new JPanel(new GridLayout(1, 5));
        formPanel = new JPanel(new GridLayout(4, 2));
        searchPanel = new JPanel(new GridLayout(1, 3));

        previousButton = new JButton("<");
        nextButton = new JButton(">");
        newButton = new JButton("*");
        deleteButton = new JButton("X");
        saveButton = new JButton("Save");
        searchButton = new JButton("Search");

        navigationLabel = new JLabel("");
        nameLabel = new JLabel("Name");
        emailLabel = new JLabel("Email");
        phoneLabel = new JLabel("Phone");

        nameTextField = new JTextField();
        emailTextField = new JTextField();
        phoneTextField = new JTextField();
        searchTextField = new JTextField();
    }

    /**
     * Adds all the UI Listeners
     */
    private void addListeners() {
        previousButton.addActionListener((ActionEvent event) -> {
            persons.previousIndex();
            updateView();
        });

        nextButton.addActionListener((ActionEvent event) -> {
            persons.nextIndex();
            updateView();
        });

        deleteButton.addActionListener((ActionEvent event) -> {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure that you want to delete that record?", "Warning", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                persons.remove();
                updateView();
            }
        });

        newButton.addActionListener((ActionEvent event) -> {
            currentMode = Mode.INSERT;

            navigationLabel.setText("Inserting ...");
            nameTextField.setText("");
            emailTextField.setText("");
            phoneTextField.setText("");

        });

        saveButton.addActionListener((ActionEvent event) -> {
            if (currentMode == Mode.UPDATE) {
                persons.update(nameTextField.getText(), emailTextField.getText(), Integer.parseInt(phoneTextField.getText()));
                updateView();
            } else if (currentMode == Mode.INSERT) {
                persons.add(nameTextField.getText(), emailTextField.getText(), Integer.parseInt(phoneTextField.getText()));
                currentMode = Mode.UPDATE;
                updateView();
            }
        });

        searchButton.addActionListener((ActionEvent event) ->{
            try {
                persons.setCurrentPerson(persons.search(searchTextField.getText()));
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null,"No result found.","Alert",JOptionPane.WARNING_MESSAGE);
            }
        });
    }

    /**
     * UI Components configuration
     */
    private void configComponents() {
        formPanel.setPreferredSize(new Dimension(400,200));
        formPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        searchPanel.setBackground(Color.decode("#bdc3c7"));
        navigationPanel.setBackground(Color.decode("#bdc3c7"));
    }

    /**
     * Adds the UI Components to the content pane
     */
    private void addComponents() {
        add(pane);
        pane.add(navigationPanel, BorderLayout.PAGE_START);
        pane.add(formPanel, BorderLayout.CENTER);
        pane.add(searchPanel, BorderLayout.PAGE_END);

        navigationPanel.add(previousButton);
        navigationPanel.add(deleteButton);
        navigationPanel.add(newButton);
        navigationPanel.add(nextButton);
        navigationPanel.add(navigationLabel);

        formPanel.add(nameLabel);
        formPanel.add(nameTextField);
        formPanel.add(emailLabel);
        formPanel.add(emailTextField);
        formPanel.add(phoneLabel);
        formPanel.add(phoneTextField);
        formPanel.add(saveButton);

        searchPanel.add(new JLabel());
        searchPanel.add(searchTextField);
        searchPanel.add(searchButton);
    }

    /**
     * Updates the UI with new text
     */
    private void updateView() {
        previousButton.setEnabled(persons.getCurrentIndex() > 0);
        nextButton.setEnabled(persons.getCurrentIndex() < persons.size() - 1);

        nameTextField.setText(persons.get().getName());
        emailTextField.setText(persons.get().getEmail());
        phoneTextField.setText(String.valueOf(persons.get().getPhone()));

        navigationLabel.setText((persons.getCurrentIndex() + 1) + "/" + persons.size());
    }

    @Override
    public void update(Observable o, Object arg) {
        updateView();
    }
}
