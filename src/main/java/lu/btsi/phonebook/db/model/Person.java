package lu.btsi.phonebook.db.model;

public class Person{
    private int id;
    private String name;
    private String email;
    private int phone;

    /**
     * Returns the Person id
     *
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * Returns the Person name
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the Person email
     *
     * @return String
     */
    public String getEmail() {
        return email;
    }

    /**
     * Returns the Person phone number
     *
     * @return int
     */
    public int getPhone() {
        return phone;
    }

    /**
     * Sets the Person id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Sets the Person name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the Person email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Sets the Person phone
     */
    public void setPhone(int phone) {
        this.phone = phone;
    }

}
